# Apple Snatcher #

A simple coding problem about an optimal path search. Good old backtracking can be used but I went for the AIMA General Search algorithm with a Breadth First search.

Changing it to depth search is trivial, iterative deepening search a bit more tricky and A* would be also easy if we can use some sick lambdas.

### Sample input ###

```
5 5
00001
00000
00000
00010
00000
```

And it will find the shortest path that hits every single `1`:

```
Optimal path of 9 steps found:
Goto: 0,0
Goto: 0,1
Goto: 0,2
Goto: 0,3
Goto: 0,4
Goto: 1,4
Goto: 2,4
Goto: 3,4
Goto: 3,3
```

### How do I get set up? ###

* Fetch the code
* Install boost 1.57 somewhere
* Pull the VS project into VS 2012. 
* Add the boost libraries as dependencies and build Test to get the tests, or Debug/Release to run the thing

* The main function is in apple_snatcher.cpp
* You can also build the test target in VS 2012 which runs all in apple_tester.cpp
* Both will build apple_snatcher.exe


### Limitations ###
* The performance test is disabled behind a define. Breadth search is guaranteed to not to get stuck in a loop but still it eats memory. It eventually finishes (btw, I used dequeue for a while to make it easier to switch from breadth to depth search and found out that it was spending half of the time releasing heap memory at the end of the test)
* The map is implemented as a vector of Nodes. First, the nodes have two flags, the one to check that they have been explored is only needed if we want to disallow retracing steps. Secondly, a std::unique_ptr< Node[] > works in C++11 but it is way harder to debug (you get heap corruptions when you get outside of the boundaries), as it doesn't seem to make any significant difference in speed I switched to an easier to understand vector.
* I never got your test data so I made some data from the description of the problem for the tests.

### Improvements ###

* Implementing the alternative search strategies in the FringeSet (depth search is just changing it with a stack, A* would need to sort the fringe using the heuristic, iterative deepening search is a bit tricker because I do not keep track of the search depth yet.
* Moving more logic of the MapFromTxt to the IMap.
* Switching the Solution from compile time polymorphism (templating) to runtime polymorphism (which means that the map will no longer be contained in the solution, this has some pros and cons)