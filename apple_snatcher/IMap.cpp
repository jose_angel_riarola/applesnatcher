#include "stdafx.h"
#include "IMap.h"

namespace problem
{
	const std::array<IMap::Movement, 4> IMap::EXPAND_ORDER = {
		{
			IMap::Movement(-1, 0),
			IMap::Movement(0, 1),
			IMap::Movement(1, 0),
			IMap::Movement(0, -1)
		}
	};
}
