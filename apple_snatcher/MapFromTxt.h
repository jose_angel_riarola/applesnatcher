#pragma once
#include "imap.h"
#include <istream>
#include <memory>

namespace problem
{
	class MapFromTxt :
		public IMap
	{
	private:
		std::vector<Node> the_map; //std::unique_ptr< Node[] > is faster but harder to debug @todo: Test and replace
		double n_apples; //It could be up to pow(numeric_limits< coordinates >, 2)
		Node& get(Point const& coordinates)
		{
			return the_map[height * coordinates.first + coordinates.second];
		}

	public:
		void reset();
		MapFromTxt(std::istream& txt);
		virtual ~MapFromTxt(void);


		Points expand(Point const& coordinates) override;
		bool solved() const override;

		friend void swap(MapFromTxt& left, MapFromTxt& right);
		MapFromTxt(MapFromTxt const& other);
		MapFromTxt& operator=(MapFromTxt other);
	};
}
