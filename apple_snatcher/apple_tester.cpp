#include "stdafx.h"

#define BOOST_TEST_MODULE AppleTest
#include <boost/test/unit_test.hpp>
#include "MapFromTxt.h"
#include "Solution.h"
#include "Solver.h"


using namespace problem;


BOOST_AUTO_TEST_CASE( a_map_cannot_be_created_from_corrupted_input )
{
	{
		std::stringstream empty_stream;
		std::unique_ptr<MapFromTxt> subject;
		BOOST_CHECK_THROW( subject.reset(new MapFromTxt(empty_stream)), IMap::MapException);
	}

	{
		std::stringstream corrupted_stream;
		corrupted_stream << "BANANA BANANA \n0000001000\n0000010";
		std::unique_ptr<MapFromTxt> subject;
		BOOST_CHECK_THROW( subject.reset(new MapFromTxt(corrupted_stream)), IMap::MapException);
	}

	{
		std::stringstream corrupted_stream;
		corrupted_stream << "1 100\n0000001000\n0000010";
		std::unique_ptr<MapFromTxt> subject;
		BOOST_CHECK_THROW( subject.reset(new MapFromTxt(corrupted_stream)), IMap::MapException);
	}

	{
		std::stringstream corrupted_stream;
		corrupted_stream << "3 3\n001\n100";
		std::unique_ptr<MapFromTxt> subject;
		BOOST_CHECK_THROW( subject.reset(new MapFromTxt(corrupted_stream)), IMap::MapException);
	}

	{
		std::stringstream corrupted_stream;
		corrupted_stream << "3 3\nABC\nDEF\n001";
		std::unique_ptr<MapFromTxt> subject;
		BOOST_CHECK_THROW( subject.reset(new MapFromTxt(corrupted_stream)), IMap::MapException);
	}

	{
		std::stringstream corrupted_stream;
		corrupted_stream << "3 3\n001\n100\n300";
		std::unique_ptr<MapFromTxt> subject;
		BOOST_CHECK_THROW( subject.reset(new MapFromTxt(corrupted_stream)), IMap::MapException);
	}
}

BOOST_AUTO_TEST_CASE( a_map_can_be_created_from_good_input )
{
	std::unique_ptr<MapFromTxt> subject;
	{
		std::stringstream ok_stream;
		ok_stream << "3 3\n000\n000\n001";
		BOOST_CHECK_NO_THROW( subject.reset( new MapFromTxt(ok_stream)));
		BOOST_CHECK_EQUAL(subject->solved(), false) ;
	}

	{
		std::stringstream ok_stream;
		ok_stream << "3 3\n000\n000\n000";
		BOOST_CHECK_NO_THROW( subject.reset(new MapFromTxt(ok_stream)));
		BOOST_CHECK_EQUAL(subject->solved(), true) ;
	}

	{
		std::stringstream ok_stream;
		ok_stream << "5 3\n00001\n00100\n00001";
		BOOST_CHECK_NO_THROW( subject.reset(new MapFromTxt(ok_stream)));
		BOOST_CHECK_EQUAL(subject->solved(), false) ;
	}

	{
		std::stringstream ok_stream;
		ok_stream << "5 3       \n00001      \n00100     \n00001";
		BOOST_CHECK_NO_THROW( subject.reset(new MapFromTxt(ok_stream)));
		BOOST_CHECK_EQUAL(subject->solved(), false) ;
	}

	{
		std::stringstream ok_stream;
		ok_stream << "5 3       \n00000      \n00000     \n00000";
		BOOST_CHECK_NO_THROW( subject.reset(new MapFromTxt(ok_stream)));
		BOOST_CHECK_EQUAL(subject->solved(), true) ;
	}
}

BOOST_AUTO_TEST_CASE( a_map_detects_invalid_coordinates )
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n000\n000\n000";
	MapFromTxt map(ok_stream);

	BOOST_CHECK( !map.are_valid_coordinates(IMap::Point(4,1))) ;
	BOOST_CHECK( !map.are_valid_coordinates(IMap::Point(2,13))) ;
	BOOST_CHECK( !map.are_valid_coordinates(IMap::Point(3,13))) ;
	BOOST_CHECK( map.are_valid_coordinates(IMap::Point(0,0))) ;
	BOOST_CHECK( map.are_valid_coordinates(IMap::Point(2,2))) ;
}

BOOST_AUTO_TEST_CASE( a_map_does_nothing_when_tries_to_expand_outside_of_itself )
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);
	IMap::Points steps = map.expand(IMap::Point(3, 3));

	BOOST_CHECK( !map.solved()) ;
	BOOST_CHECK( steps.empty()) ;
}

BOOST_AUTO_TEST_CASE( a_map_expands_clockwise )
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);
	{
		IMap::Points steps = map.expand(IMap::Point(0, 0));
		BOOST_CHECK_EQUAL( steps.size(), 2) ;
		BOOST_CHECK_EQUAL( steps[0].first, 0) ;
		BOOST_CHECK_EQUAL( steps[0].second, 1) ;
		BOOST_CHECK_EQUAL( steps[1].first, 1) ;
		BOOST_CHECK_EQUAL( steps[1].second, 0) ;
	}

	{
		IMap::Points steps = map.expand(IMap::Point(2, 2));
		BOOST_CHECK_EQUAL( steps.size(), 2) ;
		BOOST_CHECK_EQUAL( steps[0].first, 1) ;
		BOOST_CHECK_EQUAL( steps[0].second, 2) ;
		BOOST_CHECK_EQUAL( steps[1].first, 2) ;
		BOOST_CHECK_EQUAL( steps[1].second, 1) ;
	}

	{
		IMap::Points steps = map.expand(IMap::Point(1, 1));
		BOOST_CHECK_EQUAL( steps.size(), 4) ;
		BOOST_CHECK_EQUAL( steps[0].first, 0) ;
		BOOST_CHECK_EQUAL( steps[0].second, 1) ;
		BOOST_CHECK_EQUAL( steps[1].first, 1) ;
		BOOST_CHECK_EQUAL( steps[1].second, 2) ;
		BOOST_CHECK_EQUAL( steps[2].first, 2) ;
		BOOST_CHECK_EQUAL( steps[2].second, 1) ;
		BOOST_CHECK_EQUAL( steps[3].first, 1) ;
		BOOST_CHECK_EQUAL( steps[3].second, 0) ;
	}

	//I could test one that gives 3 but I am sure that it's fine without that test
}

/*
BOOST_AUTO_TEST_CASE( a_map_doesnt_expand_visited_nodes )
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);		
	{
		IMap::Points steps = map.expand(IMap::Point(0, 0));
		map.expand(steps[0]);
		BOOST_CHECK_EQUAL( steps.size(), 2);
	}

	
}
*/


BOOST_AUTO_TEST_CASE( a_map_DOES_expand_visited_nodes )
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);
	{
		IMap::Points steps = map.expand(IMap::Point(0, 0));
		map.expand(steps[0]);
		BOOST_CHECK_EQUAL( steps.size(), 2) ;
	}
}

BOOST_AUTO_TEST_CASE( a_map_solution_fails_to_expand_outside_of_itself )
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n000\n000\n000";
	MapFromTxt map(ok_stream);
	Solution<MapFromTxt> subject(map);

	BOOST_CHECK_THROW( subject.expand(IMap::Point(10, 11)), SolutionException);
}

BOOST_AUTO_TEST_CASE( a_map_solution_gets_solved_when_it_expands_the_last_apple)
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);
	Solution<MapFromTxt> start(map);

	Solution<MapFromTxt>::DescendentSolutions end = start.expand(IMap::Point(0, 1));

	BOOST_CHECK( !start.solved()) ;
	BOOST_CHECK( end[0].first.solved()) ;
}

BOOST_AUTO_TEST_CASE( a_map_solution_keeps_track_of_its_path)
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);
	Solution<MapFromTxt> start(map);

	Solution<MapFromTxt>::DescendentSolutions end = start.expand(IMap::Point(0, 1));

	BOOST_CHECK( start.get_path().empty()) ;
	BOOST_CHECK_EQUAL( end[0].first.get_path().size(), 1) ;
}

BOOST_AUTO_TEST_CASE( solve_detects_no_apples)
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n000\n000\n000";
	MapFromTxt map(ok_stream);

	Solution<MapFromTxt> result(map);
	Solver::rcode rCode = Solver::solve(result);

	BOOST_CHECK_EQUAL( rCode, Solver::NO_APPLES) ;
	BOOST_CHECK(result.solved()) ;
}

BOOST_AUTO_TEST_CASE( solve_finds_a_trivial_path)
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n000";
	MapFromTxt map(ok_stream);

	Solution<MapFromTxt> result(map);
	Solver::rcode rCode = Solver::solve(result);

	BOOST_CHECK_EQUAL( rCode, Solver::OK) ;
	BOOST_CHECK(result.solved()) ;
	BOOST_CHECK_EQUAL(result.get_path().size(), 2) ;
}

BOOST_AUTO_TEST_CASE( solve_finds_a_not_so_trivial_path)
{
	std::stringstream ok_stream;
	ok_stream << "3 3\n010\n000\n011";
	MapFromTxt map(ok_stream);

	Solution<MapFromTxt> result(map);
	Solver::rcode rCode = Solver::solve(result);

	BOOST_CHECK_EQUAL( rCode, Solver::OK) ;
	BOOST_CHECK(result.solved()) ;
	BOOST_CHECK_EQUAL(result.get_path().size(), 5) ;
}

BOOST_AUTO_TEST_CASE( solve_finds_a_not_so_trivial_non_square_path) //That was my worst bug so far
{
	std::stringstream ok_stream;
	ok_stream << "2 3\n01\n00\n01";
	MapFromTxt map(ok_stream);


	Solution<MapFromTxt> result(map);
	Solver::rcode rCode = Solver::solve(result);

	BOOST_CHECK_EQUAL( rCode, Solver::OK) ;
	BOOST_CHECK(result.solved()) ;
	BOOST_CHECK_EQUAL(result.get_path().size(), 4) ;
}


#ifdef performance_test
BOOST_AUTO_TEST_CASE( solve_performance_test )
{
	std::stringstream ok_stream;
	ok_stream << "10 3\n";
	ok_stream << "0000010000\n";
	ok_stream << "0001000000\n";
	ok_stream << "0100000000";
	MapFromTxt map(ok_stream);	

	Solution<MapFromTxt> result(map);
	Solver::rcode rCode = Solver::solve(result);
	
	BOOST_CHECK_EQUAL( rCode, Solver::OK);
	BOOST_CHECK(result.solved());
	BOOST_CHECK_EQUAL(result.get_path().size(), 5);
}
#endif
