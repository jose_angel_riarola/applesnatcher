#pragma once
#include "IMap.h"
#include <memory>
#include "MapFromTxt.h"
#include <deque>

namespace problem
{
	struct SolutionException : std::exception
	{
		SolutionException(std::string const& message):
			std::exception(message.c_str())
		{
		}

		virtual ~SolutionException()
		{
		}
	};

	template <class MapImplementation>
	class Solution
	{
	private:
		MapImplementation the_map;
		IMap::Points the_path;


	public:
		typedef std::vector<std::pair<Solution, IMap::Point>> DescendentSolutions;
		Solution(MapImplementation const& i_the_map);;

		virtual ~Solution(void)
		{
		};

		friend void swap(Solution& left, Solution& right)
		{
			using std::swap;

			swap(left.the_map, right.the_map);
			swap(left.the_path, right.the_path);
		}

		Solution(Solution const& other);;

		Solution& operator=(Solution other);


		DescendentSolutions expand(IMap::Point coordinates) const;;

		bool solved() const;

		IMap::Points const& get_path() const;
	};

	template <class MapImplementation>
	Solution<MapImplementation>::Solution(MapImplementation const& i_the_map):
		the_map(i_the_map)
	{
	}

	template <class MapImplementation>
	Solution<MapImplementation>::Solution(Solution const& other):
		the_map(other.the_map),
		the_path(other.the_path)
	{
	}

	template <class MapImplementation>
	Solution<MapImplementation>& Solution<MapImplementation>::operator=(Solution other)
	{
		swap(*this, other);

		return *this;
	}

	template <class MapImplementation>
	typename Solution<MapImplementation>::DescendentSolutions Solution<MapImplementation>::expand(IMap::Point coordinates) const
	{
		if (!the_map.are_valid_coordinates(coordinates))
		{
			throw SolutionException("Failed to expand: Invalid coordinates");
		}

		Solution result = Solution(*this);
		IMap::Points children = result.the_map.expand(coordinates);
		result.the_path.push_back(coordinates);

		DescendentSolutions results;
		for (IMap::Point step : children)
		{
			results.push_back(DescendentSolutions::value_type(result, step));
		}

		return results;
	}

	template <class MapImplementation>
	bool Solution<MapImplementation>::solved() const
	{
		return the_map.solved();
	}

	template <class MapImplementation>
	IMap::Points const& Solution<MapImplementation>::get_path() const
	{
		return the_path;
	}
}
