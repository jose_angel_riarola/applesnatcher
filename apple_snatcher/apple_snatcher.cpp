// apple_snatcher.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <string>
#include <iostream>
#include "MapFromTxt.h"
#include "Solver.h"

int main()
{
	using namespace problem;
	std::cout << "Waiting for input... (until EOF)\n";

	try
	{
		MapFromTxt in_map(std::cin);

		std::cout << "Solving...\n";

		Solution<problem::MapFromTxt> problem(in_map);

		switch (Solver::solve(problem))
		{
		case Solver::NO_SOLUTION:
			std::cout << "We couldn't find a solution to grab those juicy apples\n";
			break;
		case Solver::NO_APPLES:
			std::cout << "There are no apples to grab\n";
			break;
		case Solver::OK:
			std::cout << "Optimal path of " << problem.get_path().size() << " steps found:\n";
			for (IMap::Point point : problem.get_path())
			{
				std::cout << "Goto: " << point.first << ',' << point.second << '\n';
			}
			break;
		}
	}
	catch (IMap::MapException const& map_exception)
	{
		std::cerr << map_exception.what();
		return -1;
	}
	catch (SolutionException const& solution_exception)
	{
		std::cerr << solution_exception.what();
		return -2;
	}

	return 0;
}
