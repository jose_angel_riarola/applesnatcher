﻿#pragma once
#include "MapFromTxt.h"
#include "Solution.h"
#include <boost/optional/optional.hpp>
#include <iostream>
#include <queue>

namespace problem
{
	template <class Element>
	struct FringeSet : public std::queue<Element>
	{
		virtual ~FringeSet()
		{
		}

		//This is the strategy (breadth first). Move to an interface to make this fancy (A* and Dijstra simplifications)		
		virtual void prioritise(Element const& new_node)
		{
			push(new_node);
			//a stack would be depth_first. But that might get stuck in a cycle. Iterative deepening search is ugly to implement
		}
	};

	class Solver
	{
	private:
		typedef Solution<MapFromTxt> SolutionFromTxt;
		typedef SolutionFromTxt::DescendentSolutions::value_type SearchNode;

	public:

		enum rcode
		{
			OK,
			NO_APPLES,
			NO_SOLUTION
		};

		static rcode solve(SolutionFromTxt& problem, IMap::Point start = IMap::Point(0, 0));
	};
}
