﻿#include "stdafx.h"

#include "Solver.h"

namespace problem
{
	Solver::rcode Solver::solve(SolutionFromTxt& problem, IMap::Point start)
	{
		FringeSet<SearchNode> fringe;

		if (problem.solved())
		{
			return NO_APPLES;
		}
		fringe.push(SearchNode(problem, start));


		while (!fringe.empty())
		{
			SearchNode candidate = fringe.front();
			fringe.pop();

			if (candidate.first.solved())
			{
				problem = candidate.first;
				return OK;
			}

			for (SearchNode next : candidate.first.expand(candidate.second))
			{
				fringe.prioritise(next);
			}
		}

		return NO_SOLUTION;
	};
}
