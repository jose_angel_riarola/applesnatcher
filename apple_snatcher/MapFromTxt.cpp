#include "stdafx.h"
#include "MapFromTxt.h"

namespace problem
{
	void MapFromTxt::reset()
	{
		the_map.clear();
		the_map.resize(height * width);
	}

	MapFromTxt::MapFromTxt(std::istream& txt) : IMap(0, 0), n_apples(0)
	{
		txt >> width;
		txt >> height;

		if (width == 0 || height == 0)
		{
			throw MapException("Failed to construct: Invalid dimensions");
		}

		//Skip until newline
		txt.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		reset();

		std::unique_ptr<char[]> line(new char[width]);
		for (int i = 0; i < height; ++i)
		{
			txt.read(line.get(), width);
			if (txt.fail())
			{
				throw MapException("Failed to construct: Line had unexpected width");
			}
			txt.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

			for (int j = 0; j < width; ++j)
			{
				Node& node = the_map[i * width + j];
				switch (line[j])
				{
				case '0':
					node.has_apple = false;
					break;
				case '1':
					node.has_apple = true;
					n_apples++;
					break;
				default:
					throw MapException("Failed to construct: Unexpected character");
				}
			}
		}
	}

	MapFromTxt::~MapFromTxt(void)
	{
	}

	bool MapFromTxt::solved() const
	{
		return n_apples == 0; //other values require an epsilon
	}

	MapFromTxt::MapFromTxt(MapFromTxt const& other):
		IMap(other.width, other.height),
		n_apples(other.n_apples),
		the_map(other.the_map)
	{
		//optional if we want to use a std::unique_ptr< Node[] >
		//reset();
		//memcpy(the_map.get(), other.the_map.get(), width * height * sizeof(Node));
	}

	MapFromTxt& MapFromTxt::operator=(MapFromTxt other)
	{
		swap(*this, other);

		return *this;
	}

	void swap(MapFromTxt& left, MapFromTxt& right)
	{
		using std::swap;

		swap(left.width, right.width);
		swap(left.height, right.height);
		swap(left.n_apples, right.n_apples);
		left.the_map.swap(right.the_map);
	}

	IMap::Points MapFromTxt::expand(Point const& coordinates)
	{
		std::vector<Point> result;
		if (are_valid_coordinates(coordinates)) //Optional, the solution already checks this. We could even assert.
		{
			Node& node = get(coordinates);

			//node.explored = true; //optional
			if (node.has_apple)
			{
				node.has_apple = false;
				n_apples--;
			}

			for (size_t i = 0; i < EXPAND_ORDER.size(); ++i)
			{
				Movement const& movement = EXPAND_ORDER[i];
				Point candidate = Point(coordinates.first + movement.first, coordinates.second + movement.second);
				if (are_valid_coordinates(candidate)) //Here we can check that explored steps are not retraced (&& !get(candidate).explored)
				{
					result.push_back(candidate);
				}
			}
		}

		return result;
	}
}
