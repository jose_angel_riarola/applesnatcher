#pragma once

#include <boost/cstdint.hpp>
#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <array>

namespace problem
{
	class IMap : boost::noncopyable
	{
	public:
		typedef uint16_t coordinate;
		typedef std::pair<coordinate, coordinate> Point;
		typedef std::vector<Point> Points;

		struct Node
		{
			//As I use this Node also for the answer these two meta-data attributes
			//could be made optional to save some memory.
			bool has_apple;

			//bool		explored; //Needed if we want to disable retracing our own steps (by flagging them as explored)

			//Setting values paranoically for clarity
			Node():
				//explored(false),
				has_apple(false)
			{
			}
		};

		struct MapException : std::exception
		{
			MapException(std::string const& message):
				std::exception(message.c_str())
			{
			}

			virtual ~MapException()
			{
			}
		};

		virtual bool solved() const = 0;

		virtual bool are_valid_coordinates(Point const& coordinates) const
		{
			return coordinates.first < width && coordinates.second < height;
		};

		virtual Points expand(Point const& coordinates) = 0;

		virtual ~IMap(void)
		{
		};

	protected:
		typedef std::pair<int8_t, int8_t> Movement;
		static const std::array<Movement, 4> EXPAND_ORDER;

		coordinate width;
		coordinate height;

		IMap(coordinate i_width, coordinate i_height) : width(i_width), height(i_height)
		{
		};
	};
}
